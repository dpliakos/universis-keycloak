### Keycloak Client Service for Universis Api Server

@universis/keycloak allows a [Universis Api Server](https://gitlab.com/universis/universis) to use [Keycloak](https://www.keycloak.org/)
as backend authorization server.

#### Usage

Install @universis/keycloak as dependency of a Universis Api Server:

    npm i @universis/keycloak

#### Service Configuration

Use KeycloakClientService as service strategy of OAuth2ClientService of [Universis Api Server](https://gitlab.com/universis/universis)

server/config/app.production.json

    "services": [
            ...
            {
                "serviceType": "./services/oauth2-client-service#OAuth2ClientService",
                "strategyType": "@universis/keycloak#KeycloakClientService"
            },
            ...
        ]

Note: Check [@themost/express](https://github.com/kbarbounakis/most-data-express) version and update to ^1.3.5.

    npm update @themost/express

#### Application Configuration

Configure Universis Api Server to use Keycloak endpoints to validate user access:

    "settings": {
            ...
            "auth": {
                "server_uri":"https://<keycloak server>/auth/realms/<keycloak realm>/protocol/openid-connect/",
                "client_id":"universis-client",
                "client_secret":"secret",
            }
            ...
    }

Note: You should enable confidential access type for a keycloak client to get a client secret. 

#### Client Application Configuration

Following Universis Api Server configuration update, configure each client application respectively:

e.g. change settings#auth section of universis-students configuration

    "auth": {
        "authorizeURL":"https://<keycloak server>/auth/realms/<keycloak realm>/protocol/openid-connect/auth",
        "logoutURL":"https://<keycloak server>/auth/realms/<keycloak realm>/protocol/openid-connect/logout?redirect_uri=http://localhost:7001/#/auth/login",
        "userProfileURL":"https://<api server>/api/users/me",
        "oauth2": {
            "clientID": "universis-client",
            "callbackURL": "http://localhost:7001/auth/callback/index.html",
            "scope": [
                "students"
            ]
        }
    }
